package test

import (
	"fmt"
	l "linked-list/cmd/list"
	"testing"

	"github.com/stretchr/testify/assert"
)

const(
	testListValueCreated = "12345"
	testListValueReverse = "54321"
)

func ToString(list *l.List) string {
	var listValue string
	currentElement := list
	for {
		listValue += fmt.Sprint(currentElement.Value)
		if currentElement.Next != nil {
			currentElement = currentElement.Next
		} else {
			break
		}
	}
	return listValue
}

func TestListCreated(t *testing.T) {
	list := l.CreateList()
	listValue := ToString(list)
	if !assert.Equal(t, listValue, testListValueCreated) {
		t.Errorf("String formated was incorrect, got: %s, want: %s.", listValue, testListValueCreated)
	}
}

func TestListReverse(t *testing.T) {
	list := l.CreateList()
	list = l.ReverseList(list)
	listValue := ToString(list)
	if !assert.Equal(t, listValue, testListValueReverse) {
		t.Errorf("String formated was incorrect, got: %s, want: %s.", listValue, testListValueReverse)
	}
}
