package list

import "fmt"

// List список
type List struct {
	Next  *List //указатель на следуюшщий элемент
	Value int   //значение
}

// CreateList создание списка
func CreateList() *List {
	head := &List{nil, 1}  //начало списка
	currentElement := head // текущий элемент
	for i := 2; i <= 5; i++ {
		list := &List{nil, i}      //создание элемента списка пустым указателем на следующий элемен и значением
		currentElement.Next = list // добавление к текущему элементу указателя на следующий элемент
		currentElement = list      // устанавливаем элемент на только что добавленый (перемещение указателя на следующий элемент списка)
	}
	return head //возвращаем указатель на начало списка
}

// PrintList вывод списка на экран
func PrintList(list *List) {
	currentElement := list //устанавливаем в текущий элемент элемент начала списка
	for {                  // бесконечный цикл
		fmt.Printf("%d", currentElement.Value) //выводим значение текущего элемента
		if currentElement.Next != nil {        // если не конец списка(в конце списка указатель смотрит на nil)
			fmt.Printf(" -> ")                   // печатаем разделитель
			currentElement = currentElement.Next //устанавливаем в текущий элемент следующий в списке
		} else { // если конец списка
			break // выход из цикла
		}
	}
	fmt.Printf("\n") //перенос каретки после печати списка
}

// ReverseList перевод списка в обратный порядок
func ReverseList(list *List) *List {
	currentElement := list //устанавливаем в текущий элемент элемент начала списка
	var top *List          // создаем пустой элемент списка для указателя на предыдущий элемент списка
	for {                  // бесконечный цикл
		if currentElement == nil { // если конец списка
			break // выход из цикла
		}
		temp := currentElement.Next // создаем переменную, содержащую следующий элемент списка
		currentElement.Next = top   //
		top = currentElement        // swap текущего элемента и следующего (для первого элемента nil)
		currentElement = temp       //
	}

	return top // возвращаем новый указатель на начало списка
}
