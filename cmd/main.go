package main

func main() {
	list := CreateList()         // создаем списо
	PrintList(list)              // печатаем список
	PrintList(ReverseList(list)) // печатаем список в обратном порядке
}
